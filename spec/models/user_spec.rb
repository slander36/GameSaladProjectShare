# == Schema Information
#
# Table name: users
#
#  id         :integer         not null, primary key
#  username   :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe User do
	
	before { @user = User.new(username: "Example Tester") }

	subject { @user }

	it { should respond_to(:username) }
	it { should respond_to(:remember_token) }

	it { should be_valid }

	describe "when username is not present" do
		before { @user.username = " " }
		it { should_not be_valid }
	end

	describe "when username is already taken" do
		before do
			user_with_same_username = @user.dup
			user_with_same_username.save
		end

		it { should_not be_valid }
	end

	describe "remember token" do
		before { @user.save }
		its(:remember_token) { should_not be_blank }
	end
end
