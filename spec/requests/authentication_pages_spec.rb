require 'spec_helper'

describe "Authentication" do
	
	subject { page }

	describe "signin page" do
		before { visit signin_path }

		describe "with invalid information" do
			before { click_button "Sign In" }

			it { should have_selector('title',
			 	text: full_title("Sign In")) }
			it { should have_selector('div.alert.alert-error',
				text: 'Invalid') }

			describe "after visiting another page" do
				before { click_link 'Home' }
				it { should_not have_selector('div.alert.alert-error') }
			end
		end

		describe "with valid information" do
			before do
				fill_in 'Username', with: "foo"
				fill_in 'Password', with: "bar"
				click_button 'Sign In'
			end

			it { should have_selector('title',
				text: "foo") }
			it { should have_link('Sign Out',
				href: signout_path) }
			it { should_not have_link('Sign In',
				href: signin_path) }

			describe "followed by signout" do
				before { click_link "Sign Out" }
				it { should have_link("Sign In") }
			end
		end
	end
end
