module Jira
	class User
		include HTTParty
		base_uri "https://COMPANY.jira.com/rest/api/latest"

		def self.authenticate(username, password)
			auth = { username: username, password: password }
			Rails.logger.debug(auth)
			Rails.logger.debug(base_uri)
			response = self.get('/user.json',
				basic_auth: auth,
				query: { username: username,
					expand: 'groups' })
			Rails.logger.debug(response.code)
			200 == response.code ? response.parsed_response : false
		end
	end
end
