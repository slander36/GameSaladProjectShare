# == Schema Information
#
# Table name: users
#
#  id         :integer         not null, primary key
#  username   :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class User < ActiveRecord::Base
  attr_accessible :username

	validates :username,
		presence: true,
		uniqueness: true

	before_save :create_remember_token

	private
		
		def create_remember_token
			self.remember_token = SecureRandom.urlsafe_base64
		end
end
