class SessionsController < ApplicationController
	require 'jira'

	def new

	end

	def create
		username = params[:session][:username]
		password = params[:session][:password]
		if Jira::User.authenticate(username, password)
			if User.find_by_username(username)
				@user = User.find_by_username(username)
			else
				@user = User.create({username: username})
			end
			if sign_in @user
				flash[:notify] = "Signed In"
				redirect_to @user
			else
				flash.now[:error] = "Error signing in"
				render 'new'
			end
		else
			flash.now[:error] = "Invalid username/password"
			render 'new'
		end
	end

	def destroy
		sign_out
		redirect_to root_path
	end

end
